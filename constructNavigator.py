# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Created by Benjamin Bolling, Main Control Room Shift Leader, European Spallation Source, ERIC, Lund, Sweden.
# 2021-01-22 -> 2021-03-10
# Advanced part with GUI rebuild: 2021-08-18 -> 2021-08-20
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# This function initiates the construction of the Navigator OPI. Only things needed to modify are here.
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

from PyQt5.QtWidgets import QApplication,QCheckBox,QComboBox,QDialog,QDoubleSpinBox,QFileDialog,QGridLayout,QHeaderView,QInputDialog,QMessageBox,QLabel,QLineEdit,QPushButton,QRadioButton,QScrollArea,QSpinBox,QTableWidget,QTableWidgetItem,QWidget
from PyQt5.QtCore import Qt
from copy import deepcopy
import sys
import navigatorIO as IO

class constructNavigatorWdg(QWidget):
    def __init__(self, parent=None):
        super(constructNavigatorWdg, self).__init__(parent)
        self.setWindowTitle("Construct Navigator")
        START_DIR = QFileDialog.getExistingDirectory(self, "Select OPIs Directory")
        if not START_DIR: START_DIR = '/opt/opis/30-Operator'
        self.itemInfo,self.max_lvls,self.cwd,self.cwd_origin = IO.getItemInfo(START_DIR=START_DIR)
        self.create_frame()

    def create_layout(self):
        opilvl_lbl = QLabel("OPIs level: ")
        opilvl_lbl.setAlignment(Qt.AlignRight|Qt.AlignVCenter)
        self.lvl = QComboBox()
        self.lvl.addItem("30-Operator")
        self.lvl.addItem("20-SystemExpert")
        self.layout.addWidget(opilvl_lbl,0,0,1,1)
        self.layout.addWidget(self.lvl,0,1,1,1)

        wdt_lbl = QLabel("Full width [px]: ")
        wdt_lbl.setAlignment(Qt.AlignRight|Qt.AlignVCenter)
        self.wdt = QSpinBox()
        self.wdt.setRange(1000,1000000)
        self.wdt.setValue(2520) # 2800 ,
        self.layout.addWidget(wdt_lbl,0,2,1,1)
        self.layout.addWidget(self.wdt,0,3,1,1)

        hgt_lbl = QLabel("Full height [px]: ")
        hgt_lbl.setAlignment(Qt.AlignRight|Qt.AlignVCenter)
        self.hgt = QSpinBox()
        self.hgt.setRange(600,1000000)
        self.hgt.setValue(1400)
        self.layout.addWidget(hgt_lbl,0,4,1,1)
        self.layout.addWidget(self.hgt,0,5,1,1)

        self.runbtn = QPushButton("Create Navigator")
        self.runbtn.clicked.connect(self.constructNavigator)
        self.layout.addWidget(self.runbtn,0,6,1,1)

        self.loadbtn = QPushButton("Load Configuration")
        self.loadbtn.clicked.connect(self.loadConf)
        self.loadbtn.setToolTip("OPIs in list file are selected.")
        self.layout.addWidget(self.loadbtn,2,3,1,2)

        self.savebtn = QPushButton("Save Configuration")
        self.savebtn.clicked.connect(self.saveConf)
        self.savebtn.setToolTip("OPIs Selected sent to a list file.")
        self.layout.addWidget(self.savebtn,2,5,1,2)

    def saveConf(self):
        fn, type = QFileDialog.getSaveFileName(self, 'Save Configuration to File', "Untitled.conf", "Configuration Files (*.conf)", options=QFileDialog.DontUseNativeDialog)
        if len(fn) > 0:
            linksIncluded = self.redefine_IDs()
            f = open(fn,'w')
            f.write('{},{},{}'.format(self.wdt.value(),self.hgt.value(),self.lvl.currentText()))
            for row,key in enumerate(linksIncluded.keys()):
                if linksIncluded[key][-1] != '---':
                    f.write('\n{}'.format(linksIncluded[key][-1]))
            f.close()

    def loadConf(self):
        fn, type = QFileDialog.getOpenFileName(self, "Load Configuration from File", "", "Configuration Files (*.conf)", options=QFileDialog.DontUseNativeDialog)
        if len(fn) > 0:
            f = open(fn, "r").read().split('\n')
            settings,contents = f[0],f[1:]

            self.wdt.setValue(int(settings.split(',')[0]))
            self.hgt.setValue(int(settings.split(',')[1]))
            self.lvl.setCurrentText(settings.split(',')[2])

            # Select all
            for i,key in enumerate(self.itemInfo.keys()):
                if self.tableWdg.cellWidget(i, 0).isChecked() is False:
                    self.tableWdg.cellWidget(i, 0).setChecked(True)
            for i,key in enumerate(self.itemInfo.keys()):
                if self.tableWdg.cellWidget(i, 4).text() != '---' and self.tableWdg.cellWidget(i, 4).text() not in contents:
                    self.tableWdg.cellWidget(i, 0).setChecked(False)

    def create_table_layout(self,rows):
        self.tableArea = QGridLayout(self)
        self.tableWdg = QWidget()
        self.tableWdg.setLayout(self.tableArea)

        #   Scroll Area Properties
        self.scroll = QScrollArea()
        self.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll.setWidgetResizable(True)
        self.scroll.setWidget(self.tableWdg)

        self.tableArea = QGridLayout(self)
        self.tableArea.addWidget(self.scroll)

        self.tableLayout = QGridLayout(self)
        self.tableLayout.addWidget(self.scroll)
        self.tableLayout.setContentsMargins(0, 0, 0, 0)

        self.tableWdg = QTableWidget()
        self.tableWdg.setRowCount(rows)
        cols = 5
        self.tableWdg.setColumnCount(cols)
        self.tableWdg.setHorizontalHeaderLabels(["Include:","ID","Text:","Parent ID:","Link:"])

        self.populate_table()

        header = self.tableWdg.horizontalHeader()
        for i in range(cols):
            header.setSectionResizeMode(i, QHeaderView.ResizeToContents)
        self.tableLayout.addWidget(self.tableWdg,0,0,1,1)

    def populate_table(self):
        for row,key in enumerate(self.itemInfo.keys()):
            node_chk = QCheckBox("Include")
            node_chk.setChecked(True)
            node_chk.stateChanged.connect(self.node_chk_stateChange)
            node_text = QLineEdit(self.itemInfo[key][0])
            node_text.setMaxLength(10)
            node_text.setAlignment(Qt.AlignCenter)
            node_id = QLabel(key)
            node_id.setAlignment(Qt.AlignCenter)
            node_parent = QLabel(str(self.itemInfo[key][1]))
            node_parent.setAlignment(Qt.AlignCenter)
            node_link = QLabel(str(self.itemInfo[key][-1]))
            node_link.setAlignment(Qt.AlignLeft|Qt.AlignVCenter)
            self.tableWdg.setCellWidget(row,0,node_chk)
            self.tableWdg.setCellWidget(row,1,node_id)
            self.tableWdg.setCellWidget(row,2,node_text)
            self.tableWdg.setCellWidget(row,3,node_parent)
            self.tableWdg.setCellWidget(row,4,node_link)

    def node_chk_stateChange(self):
        for i,key in enumerate(self.itemInfo.keys()):
            if self.tableWdg.cellWidget(i, 0).isChecked() is False:
                for j,child_key in enumerate(self.itemInfo.keys()):
                    if int(self.itemInfo[child_key][1]) == int(key):
                        self.tableWdg.cellWidget(j, 0).setChecked(False)

    def create_frame(self):
        self.layout = QGridLayout(self)
        self.create_layout()
        self.create_table_layout(len(self.itemInfo))
        self.layout.addLayout(self.tableLayout,1,0,1,7)
        self.setLayout(self.layout)

    def redefine_IDs(self):
        itemInfo = {}
        itemInfo_new = {}
        for i,key in enumerate(self.itemInfo.keys()):
            if self.tableWdg.cellWidget(i, 0).isChecked() is True:
                itemInfo[key] = deepcopy(self.itemInfo[key])
                itemInfo[key][0] = self.tableWdg.cellWidget(i, 2).text()

        keys_old = list(itemInfo.keys())
        vals = list(itemInfo.values())

        for i,key in enumerate(keys_old):
            if i != key:
                vals = IO.replace_key(deepcopy(vals),key,str(i))
            itemInfo_new[str(i)] = vals[i]

        return itemInfo_new

    def constructNavigator(self):
        itemInfo = self.redefine_IDs()
        IO.buildNavigatorOPI(itemInfo,self.wdt.value(),self.hgt.value(),self.max_lvls,self.cwd,self.lvl.currentText(),self.cwd_origin)
        print("Done! Navigator OPI created from OPIs present in: >>  {}".format(self.cwd))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = constructNavigatorWdg()
    window.show()
    sys.exit(app.exec_())
