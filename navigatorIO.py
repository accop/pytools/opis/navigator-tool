# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Created by Benjamin Bolling, Main Control Room Shift Leader, European Spallation Source, ERIC, Lund, Sweden.
# 2021-01-22 -> 2021-03-10
# Advanced part with GUI rebuild: 2021-08-18 -> 2021-08-20
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# All IO functions needed in the Navigator OPI generating Python script.
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#   Methodology:
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#
# Local PV:s for the Navigator OPI are:
#   - loc://PVID_Navigator_{}_selected_embedded
#   - loc://PVID_Navigator_{}_selected_btn
#   - loc://PVID_Navigator_{}_lvl{}    where {} is the btn level (i.e. one PV per level)
# resulting in a total of 2+n local PVs with n being the number of btn levels.
#
#
#
#   Text to update:
#
# 1. Creating a tree-like dictionary structure of the OPIs with value being the directory path to the OPI (.bob)
# 2. Assign all .bob files to a unique button ID-number (the number being the key, and value being the directory path to the OPI (.bob) and how many buttons are visible)
# 3. The unique button ID-number will be identified by the rules of each button by using a local PV (loc://PVID_Navigator) to show/hide and move the buttons up/down
# 4. Only the buttons opening other buttons (i.e. folders) write to loc://PVID_Navigator
# 5. Each button ID-number contains information on its label, which ID its parent has, and how many children it has
# 6. Buttons with ID number higher than the value of loc://PVID_Navigator will move down
# 7. Buttons show only if their parent's ID is shown and higher level buttons
# x. OPI linking buttons have ID numbers as well : loc://PVID_OPI
# x. The number of pixels to move down is 40 * number-of-fields
#
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

import os
import xmlStruct as xmlS
from copy import deepcopy

def buildNavigatorOPI(itemInfo,wdt,hgt,max_lvls,cwd,OPI_lvl,cwd_origin):
    max_objs = 0 # number of buttons in the largest configuration will serve as min value for navigator height
    # The main construction script function
    above_dict, fullpath = aboveDict(deepcopy(itemInfo))
    cwd_origin = os.path.join(cwd_origin,'opi')
    em_paths = [os.path.join(cwd_origin,'support','em0{}'.format(i)) for i in range(1,4)]

    max_lvls = get_max_lvl(itemInfo)
    
    for k, v in above_dict.items():
        above_dict[k] = sorted(v, key=int)

    verify_directories(cwd_origin,em_paths)
    create_launch_app(cwd_origin)
    create_starting_header(wdt,OPI_lvl,em_paths[0])
    create_welcome_screen(wdt,hgt,em_paths[2],max_lvls,OPI_lvl)
    create_launchSettings_embedded(os.path.join(cwd_origin,'support'),max_lvls)
    create_collected_screen(os.path.join(cwd_origin,'support'),wdt,hgt,max_lvls)
    create_embedded_container(os.path.join(cwd_origin,'support'),OPI_lvl)

    for id, data in itemInfo.items():
        create_menubar(id,itemInfo,em_paths[1],above_dict[id],max_lvls,hgt)
        if data[-1] != '---':
            create_header(id,itemInfo,wdt,OPI_lvl,em_paths[0])
            create_embedded(id,itemInfo,wdt,hgt,em_paths[2],max_lvls)
        max_objs = len(above_dict[id]) if max_objs < len(above_dict[id]) else max_objs
    
    create_settings_embedded(os.path.join(cwd_origin,'support'),max_objs)

def get_max_lvl(itemInfo):
    max_lvl = 0
    for v in itemInfo.values():
        if v[3] > max_lvl:
            max_lvl = v[3]
    return max_lvl+1

def verify_directories(destination,paths):
    for path in paths:
        os.makedirs(path, exist_ok=True)

def replace_key(listItem,old,new):
        for item in listItem:
            if item[1] == old:
                item[1] = new
        return listItem

def create_launchSettings_embedded(destination,max_lvls):
    f = open("{}/navigator_settings_embeddedLaunch.bob".format(destination),"w")
    xmlS.write_launchsettings(f,max_lvls)
    f.close()

def create_settings_embedded(destination,max_objs):
    f = open("{}/navigator_settings.bob".format(destination),"w")
    xmlS.write_settings(f,max_objs)
    f.close()

def create_collected_screen(destination,wdt,hgt,max_lvls):
    f = open("{}/navigator_macros.bob".format(destination),"w")
    xmlS.write_collected(f,wdt,hgt,max_lvls)
    f.close()

def create_starting_header(wdt,OPI_lvl,destination):
    f = open("{}/idX.bob".format(destination),"w")
    xmlS.write_starting_titlebar(f, OPI_lvl, wdt)
    f.close()

def create_welcome_screen(wdt,hgt,destination,max_lvls, OPI_lvl):
    f = open("{}/idX.bob".format(destination),"w")
    xmlS.write_embedded_welcome(f, wdt, hgt, max_lvls, OPI_lvl)
    f.close()
    pass

def create_launch_app(destination):
    f = open(destination+"/navigator.bob","w")
    xmlS.buildMainLaunchDisplay(f)
    f.close()

def create_embedded_container(destination,lvl):
    f = open(destination+"/navigator.bob","w")
    xmlS.buildMainMacroContainer(f,lvl)
    f.close()

def create_header(id,itemInfo,wdt,OPI_lvl,destination):
    parts = itemInfo[id][-1].split(OPI_lvl)[-1].split('/')
    name = parts[-1].split('.bob')[0]
    if '' in parts:
        parts.remove('')
    parts.remove('10-Top')
    text = ' > '.join(parts)
    parts = [p.split('-')[-1] for p in parts[0:-1]]
    parts.append(name)
    text = ' > '.join(parts)
    link = itemInfo[id][-1]
    f = open("{}/id{:03d}.bob".format(destination,int(id)),"w")
    xmlS.write_titlebar(f, text, wdt)
    f.close()

def create_menubar(id,itemInfo,destination,id_list,max_lvls,fullhgt):
    f = open("{}/id{:03d}.bob".format(destination,int(id)),"w")
    xmlS.write_menubar(f, itemInfo, id, id_list, max_lvls, fullhgt)
    f.close()

def create_embedded(id,itemInfo,wdt,hgt,destination, max_lvls):
    link = itemInfo[id][-1]
    f = open("{}/id{:03d}.bob".format(destination,int(id)),"w")
    xmlS.write_embedded(f, wdt, hgt, max_lvls, link, id)
    f.close()

def getItemInfo(START_DIR):
    # Create the itemInfo dict item with all nodes
    cwd_origin = os.getcwd()
    os.chdir(START_DIR)
    recursive_dict = {}
    itemInfo = {}
    max_lvls = 0
    rel_loc = '$(OPI_LOC)/'
    cwd = os.getcwd()
    OPI_LOC,rootName = os.path.split(cwd)
    recursive_dict,itemInfo,max_lvls = constructRecursiveList(cwd,rootName,recursive_dict,max_lvls,rel_loc,itemInfo)
    recursive_dict = sortRecursiveDict(deepcopy(recursive_dict))
    recursive_dict = replaceRecursiveDictKeys(deepcopy(recursive_dict))
    itemInfo = constructNodes(recursive_dict,deepcopy(itemInfo),max_lvls)
    return itemInfo,max_lvls,cwd,cwd_origin

def sortRecursiveDict(dict_in):
    # Sorting the dict (recursion function)
    return {k: sortRecursiveDict(v) if isinstance(v, dict) else v
            for k, v in sorted(dict_in.items())}

def replaceRecursiveDictKeys(dict_in):
    dict_out = {}
    for k, v in dict_in.items():
        if isinstance(v, dict):
            v = replaceRecursiveDictKeys(v)
        dict_out[rmv_not_needed_things(k)] = v
    return dict_out

def constructRecursiveList(cwd,rootName,recursive_dict,max_lvls,rel_loc,itemInfo):
    # Creating a tree-like dictionary structure of the OPIs with value being the directory path to the OPI (.bob)
    for root, directories, filenames in os.walk(cwd):
        fns = []
        for filename in filenames:
            if filename.endswith(('.bob','.opi')) and '10-Top' in str(root) and rootName in str(root) and '99-Shared' not in str(root):
                if len(str(root).split('Cryo')) <= 2: # hack to avoid the unstructured cryo OPIs clogging
                    filename = filename.replace('&','&amp;')
                    print(filename)
                    itemname = '.'.join(filename.split('.').pop(-1))
                    dict_loc = '{}/{}'.format(rmv_not_needed_MLs(str(root.split(rootName)[1])),filename.split('.bob')[0])
                    # dict_loc = '{}/{}'.format(str(root.split(rootName)[1]),filename.split('.bob')[0])
                    file_loc = str(os.path.join(rel_loc+rootName+root.split(rootName)[1],filename))
                    opipath = dict_loc.split('/')
                    if len(opipath)-1 > max_lvls:
                        max_lvls = len(opipath)-1
                    opipath.append(file_loc)
                    gen_recursive_list(recursive_dict, opipath)
    return recursive_dict,itemInfo,max_lvls

def constructNodes(recursive_dict,itemInfo,max_lvls):
    # Using the recursive dict, a new dictionary is constructed in which all items in the recursive dictionary are given unique ID:s used
    # as keys. All nodes are using the genAllNodes function.
    for ind,key in enumerate(list(recursive_dict.keys())):
        if isinstance(recursive_dict[key],dict):
            tempDict = recursive_dict[key]
            ID = str(len(itemInfo.keys()))
            itemInfo[ID] = [key,-1,len(list(recursive_dict[key].keys())),0,'---'] # label, parent ID number, number of children, level, location
            itemInfo = genAllNodes(tempDict,itemInfo,1,ID,max_lvls)
        else:
            ID = str(len(itemInfo.keys()))
            itemInfo[ID] = [key,-1,len(list(tempDict.keys())),0,str(tempDict[key])] # label, parent ID number, number of children, level, location
            print('hmmmm..... this should not happen. Contact code maintainer immediately with file structure and ID = '+str(ID))
    return itemInfo

def rmv_not_needed_MLs(inp):
    # Remove 10-top from dict chains
    out2 = inp.split('/')
    out2.remove('10-Top')
    toincl = []
    for outs in out2:
        if len(outs) > 0:
            toincl.append(outs)
    out = '/'.join(toincl)
    return out

def rmv_not_needed_things(inp):
    # Remove 10-top and id-numbers for all disciplines and systems (e.g. 999-PBI -> PBI) - but ONLY for numericals!!
    out2 = inp.split('/')
    out3 = []
    for m in out2:
        out4 = m.split('-')
        if len(out4) > 1 and out4[0].isnumeric():
            del out4[0]
        out3.append('-'.join(out4))
    toincl = []
    for outs in out3:
        if len(outs) > 0:
            toincl.append(outs)
    out = '/'.join(toincl)
    return out

def gen_recursive_list(recursive_dict, opipath):
    # A simple function generating a recursive list. Note that recursive_dict is automatically returned as it is a dictionary and this is Python.
    if len(opipath) == 0:
        return
    elif len(opipath) == 1:
        recursive_dict.append(opipath[0])
    else:
        gen_recursive_list(recursive_dict.setdefault(opipath[0], [] if len(opipath) == 2 else {}), opipath[1:])

def genAllNodes(tempDict,itemInfo,lvl,parentID,max_lvls):
    # Using the recursive dict, a new dictionary is constructed in which all items in the recursive dictionary are given unique ID:s used as keys. OPIs with the
    # name overview.bob are assigned to their parents as locations instead of added to the dictionary.
    for ind,key in enumerate(list(tempDict.keys())):
        ID = str(len(itemInfo.keys()))
        if isinstance(tempDict[key],dict):
            loc = '---'
        else:
            loc = str(tempDict[key][0])
        if str(key.lower()) == 'overview':
            itemInfo[parentID][4] = loc
        elif str(key.lower()) == 'main' and 'overview' not in list(tempDict.keys()):
            itemInfo[parentID][4] = loc
        elif str(key.lower()) == 'top' and 'overview' not in list(tempDict.keys()) and 'main' not in list(tempDict.keys()):
            itemInfo[parentID][4] = loc
        elif 'main' in str(key.lower()) and 'maintenance' not in str(key.lower()) and 'overview' not in list(tempDict.keys()) and 'main' not in list(tempDict.keys()) and 'top' not in list(tempDict.keys()):
            itemInfo[parentID][4] = loc
        elif 'overview' in str(key.lower()) and 'overview' not in list(tempDict.keys()) and 'main' not in list(tempDict.keys()) and 'top' not in list(tempDict.keys()):
            itemInfo[parentID][4] = loc

        # elif any("main" in s for s in list(tempDict.keys())):
        #     itemInfo[parentID][4] = loc
        # elif any("overview" in s for s in list(tempDict.keys())):
        #     itemInfo[parentID][4] = loc
        else:
            itemInfo[ID] = [key,parentID,len(list(tempDict.keys())),lvl,loc] # label, parent ID number, number of children, level, location
        if isinstance(tempDict[key],dict):
            itemInfo = genAllNodes(tempDict[key],itemInfo,lvl+1,ID,max_lvls)
    return itemInfo

def constructLinkToLevelButton(f,wdt,max_lvls,OPI_lvl):
    # The function for building a button to launch the Nagivator for Expert level OPIs.
    if OPI_lvl == "30-Operator":
        linklbl = "Expert"
        link = "20-SystemExpert"
    elif OPI_lvl == "20-SystemExpert":
        linklbl = "Operator"
        link = "30-Operator"
    f.write('  <widget type="action_button" version="3.0.0">\n')
    f.write('    <name>expertLink</name>\n')
    f.write('    <actions execute_as_one="true">\n')
    # Actions : PVs to write to are lvls and OPI selected # # # # # # # # # # #
    for n in range(0,max_lvls+1): # here we write to all PVs in lvl >= button clicked level (-1 to any higher level buttons)
        f.write('      <action type="write_pv">\n')
        f.write('        <pv_name>loc://PVID_Navigator_{}_lvl{}</pv_name>\n'.format(linklbl,n))
        f.write('        <value>-1</value>\n')
        f.write('        <description>Write PV lvl {}</description>\n'.format(n))
        f.write('      </action>\n')
    f.write('      <action type="open_display">\n')
    f.write('        <file>/opt/opis/{}/00-Navigator/navigator.bob</file>\n'.format(link))
    f.write('        <target>replace</target>\n')
    f.write('        <description>Replace - open {}</description>\n'.format(linklbl))
    f.write('      </action>\n')
    f.write('    </actions>\n')
    f.write('    <text>{} Navigator</text>\n'.format(linklbl))
    f.write('    <x>{}</x>\n'.format(wdt-160))
    f.write('    <y>10</y>\n')
    f.write('    <width>150</width>\n')
    f.write('    <height>30</height>\n')
    f.write('    <tooltip>$(actions)</tooltip>\n')
    f.write('    <rules>\n')
    f.write('      <rule name="xpos" prop_id="x" out_exp="true">\n')
    f.write('        <exp bool_exp="pvInt0&gt;639">\n')
    f.write('          <expression>pvInt0-160</expression>\n')
    f.write('        </exp>\n')
    f.write('        <exp bool_exp="pvInt0&lt;640">\n')
    f.write('          <expression>480</expression>\n')
    f.write('        </exp>\n')
    f.write('        <pv_name>loc://PVID_Navigator_{}_wdt({})</pv_name>\n'.format(OPI_lvl.split('-')[1],wdt))
    f.write('      </rule>\n')
    f.write('    </rules>\n')
    f.write('  </widget>\n')

def aboveDict(itemInfo):
    # A function for creating a dictionary for finding out how many buttons are shown above any button
    above_dict = {}
    fullpath_dict = {}
    for key in list(itemInfo.keys()):
        fullpath = str(itemInfo[key][0])
        above_dict[key] = []
        lvl = itemInfo[key][3]
        parentkey = key
        children = getChildren(deepcopy(itemInfo),parentkey)
        while lvl > -1:
            children = getChildren(deepcopy(itemInfo),parentkey)
            for child in children:
                above_dict[key].append(child)
            if lvl > 0:
                lvl = itemInfo[parentkey][3] # parent level
                parentkey = itemInfo[parentkey][1] # parent key
                if str(parentkey) != '-1':
                    fullpath = str(itemInfo[parentkey][0]) + ' &gt; ' + fullpath
            else:
                parentkey = '-1'
                children = getChildren(deepcopy(itemInfo),parentkey)
                for child in children:
                    if child not in above_dict[key]:
                        above_dict[key].append(child)
                lvl = -1
        fullpath_dict[key] = fullpath
    return above_dict, fullpath_dict

def getChildren(dict_in,parentKey):
    # A function for finding all of an object with key parentKey in dictionary dict_in
    list_of_children = []
    for key in list(dict_in.keys()):
        if str(parentKey) == str(dict_in[key][1]):
            list_of_children.append(key)
    return list_of_children

def buildSettingsGroup(f,OPI_lvl,hgt,wdt,rsp):
    f.write('  <widget type="group" version="2.0.0">\n')
    f.write('    <name>settingsGroup</name>\n')
    f.write('    <x>220</x>\n')
    f.write('    <y>80</y>\n')
    f.write('    <width>415</width>\n')
    f.write('    <height>305</height>\n')
    f.write('    <visible>false</visible>\n')
    f.write('    <style>3</style>\n')
    f.write('    <background_color>\n')
    f.write('      <color name="BACKGROUND" red="220" green="225" blue="221">\n')
    f.write('      </color>\n')
    f.write('    </background_color>\n')
    f.write('    <transparent>true</transparent>\n')
    f.write('    <widget type="rectangle" version="2.0.0">\n')
    f.write('      <name>shadow</name>\n')
    f.write('      <x>5</x>\n')
    f.write('      <y>5</y>\n')
    f.write('      <width>410</width>\n')
    f.write('      <height>300</height>\n')
    f.write('      <line_width>0</line_width>\n')
    f.write('      <line_color>\n')
    f.write('        <color name="GROUP-BORDER" red="150" green="155" blue="151">\n')
    f.write('        </color>\n')
    f.write('      </line_color>\n')
    f.write('      <background_color>\n')
    f.write('        <color red="0" green="0" blue="0" alpha="85">\n')
    f.write('        </color>\n')
    f.write('      </background_color>\n')
    f.write('      <corner_width>10</corner_width>\n')
    f.write('      <corner_height>10</corner_height>\n')
    f.write('    </widget>\n')
    f.write('    <widget type="rectangle" version="2.0.0">\n')
    f.write('      <name>outerBackground</name>\n')
    f.write('      <width>410</width>\n')
    f.write('      <height>300</height>\n')
    f.write('      <line_width>2</line_width>\n')
    f.write('      <line_color>\n')
    f.write('        <color name="GROUP-BORDER" red="150" green="155" blue="151">\n')
    f.write('        </color>\n')
    f.write('      </line_color>\n')
    f.write('      <background_color>\n')
    f.write('        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">\n')
    f.write('        </color>\n')
    f.write('      </background_color>\n')
    f.write('      <corner_width>10</corner_width>\n')
    f.write('      <corner_height>10</corner_height>\n')
    f.write('    </widget>\n')
    f.write('    <widget type="rectangle" version="2.0.0">\n')
    f.write('      <name>innerBackground</name>\n')
    f.write('      <x>10</x>\n')
    f.write('      <y>50</y>\n')
    f.write('      <width>390</width>\n')
    f.write('      <height>190</height>\n')
    f.write('      <line_width>2</line_width>\n')
    f.write('      <line_color>\n')
    f.write('        <color name="GROUP-BORDER" red="150" green="155" blue="151">\n')
    f.write('        </color>\n')
    f.write('      </line_color>\n')
    f.write('      <background_color>\n')
    f.write('        <color name="BACKGROUND" red="220" green="225" blue="221">\n')
    f.write('        </color>\n')
    f.write('      </background_color>\n')
    f.write('      <corner_width>5</corner_width>\n')
    f.write('      <corner_height>5</corner_height>\n')
    f.write('    </widget>\n')
    f.write('    <widget type="label" version="2.0.0">\n')
    f.write('      <name>hgt_lbl</name>\n')
    f.write('      <text>Window Height:</text>\n')
    f.write('      <x>60</x>\n')
    f.write('      <y>70</y>\n')
    f.write('      <width>120</width>\n')
    f.write('      <height>30</height>\n')
    f.write('      <horizontal_alignment>2</horizontal_alignment>\n')
    f.write('      <vertical_alignment>1</vertical_alignment>\n')
    f.write('    </widget>\n')
    f.write('    <widget type="label" version="2.0.0">\n')
    f.write('      <name>wdt_lbl</name>\n')
    f.write('      <text>Window Width:</text>\n')
    f.write('      <x>60</x>\n')
    f.write('      <y>110</y>\n')
    f.write('      <width>120</width>\n')
    f.write('      <height>30</height>\n')
    f.write('      <horizontal_alignment>2</horizontal_alignment>\n')
    f.write('      <vertical_alignment>1</vertical_alignment>\n')
    f.write('    </widget>\n')
    f.write('    <widget type="label" version="2.0.0">\n')
    f.write('      <name>rsp_lbl</name>\n')
    f.write('      <text>Resize Policy:</text>\n')
    f.write('      <x>60</x>\n')
    f.write('      <y>150</y>\n')
    f.write('      <width>120</width>\n')
    f.write('      <height>30</height>\n')
    f.write('      <horizontal_alignment>2</horizontal_alignment>\n')
    f.write('      <vertical_alignment>1</vertical_alignment>\n')
    f.write('    </widget>\n')
    f.write('    <widget type="spinner" version="2.0.0">\n')
    f.write('      <name>hgt</name>\n')
    f.write('      <pv_name>loc://PVID_Navigator_{}_hgt({})</pv_name>\n'.format(OPI_lvl.split('-')[1],int(hgt)))
    f.write('      <x>190</x>\n')
    f.write('      <y>70</y>\n')
    f.write('      <width>150</width>\n')
    f.write('      <height>30</height>\n')
    f.write('      <minimum>480.0</minimum>\n')
    f.write('      <maximum>180000.0</maximum>\n')
    f.write('      <limits_from_pv>false</limits_from_pv>\n')
    f.write('    </widget>\n')
    f.write('    <widget type="spinner" version="2.0.0">\n')
    f.write('      <name>wdt</name>\n')
    f.write('      <pv_name>loc://PVID_Navigator_{}_wdt({})</pv_name>\n'.format(OPI_lvl.split('-')[1],int(wdt)))
    f.write('      <x>190</x>\n')
    f.write('      <y>110</y>\n')
    f.write('      <width>150</width>\n')
    f.write('      <height>30</height>\n')
    f.write('      <minimum>640.0</minimum>\n')
    f.write('      <maximum>180000.0</maximum>\n')
    f.write('      <limits_from_pv>false</limits_from_pv>\n')
    f.write('    </widget>\n')
    f.write('    <widget type="combo" version="2.0.0">\n')
    f.write('      <name>rsp</name>\n')
    f.write('      <pv_name>loc://PVID_Navigator_{}_rsp({})</pv_name>\n'.format(OPI_lvl.split('-')[1],rsp))
    f.write('      <x>190</x>\n')
    f.write('      <y>150</y>\n')
    f.write('      <width>150</width>\n')
    f.write('      <height>30</height>\n')
    f.write('      <items>\n')
    f.write('        <item>No Resize</item>\n')
    f.write('        <item>Size content to fit widget</item>\n')
    f.write('        <item>Stretch content to fit widget</item>\n')
    f.write('      </items>\n')
    f.write('    </widget>\n')
    f.write('    <widget type="action_button" version="3.0.0">\n')
    f.write('      <name>Close Settings Panel</name>\n')
    f.write('      <actions>\n')
    f.write('        <action type="execute">\n')
    f.write('          <script file="EmbeddedPy">\n')
    f.write('            <text><![CDATA[# Embedded python script\n')
    f.write('from org.csstudio.display.builder.runtime.script import ScriptUtil\n')
    f.write('ScriptUtil.findWidgetByName(widget,"settingsGroup").setPropertyValue("visible", False)]]></text>\n')
    f.write('          </script>\n')
    f.write('          <description>Close Settings Display</description>\n')
    f.write('        </action>\n')
    f.write('      </actions>\n')
    f.write('      <text>Close</text>\n')
    f.write('      <x>150</x>\n')
    f.write('      <y>252</y>\n')
    f.write('      <width>110</width>\n')
    f.write('      <tooltip>$(actions)</tooltip>\n')
    f.write('    </widget>\n')
    f.write('    <widget type="label" version="2.0.0">\n')
    f.write('      <name>Title</name>\n')
    f.write('      <text>Navigator OPI Settings</text>\n')
    f.write('      <width>410</width>\n')
    f.write('      <height>50</height>\n')
    f.write('      <font>\n')
    f.write('        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">\n')
    f.write('        </font>\n')
    f.write('      </font>\n')
    f.write('      <foreground_color>\n')
    f.write('        <color name="HEADER-TEXT" red="0" green="0" blue="0">\n')
    f.write('        </color>\n')
    f.write('      </foreground_color>\n')
    f.write('      <horizontal_alignment>1</horizontal_alignment>\n')
    f.write('      <vertical_alignment>1</vertical_alignment>\n')
    f.write('      <wrap_words>false</wrap_words>\n')
    f.write('    </widget>\n')
    f.write('    <widget type="polyline" version="2.0.0">\n')
    f.write('      <name>Polyline</name>\n')
    f.write('      <x>20</x>\n')
    f.write('      <y>25</y>\n')
    f.write('      <width>70</width>\n')
    f.write('      <height>1</height>\n')
    f.write('      <points>\n')
    f.write('        <point x="0.0" y="0.0">\n')
    f.write('        </point>\n')
    f.write('        <point x="70.0" y="0.0">\n')
    f.write('        </point>\n')
    f.write('      </points>\n')
    f.write('      <line_color>\n')
    f.write('        <color name="GROUP-BORDER" red="150" green="155" blue="151">\n')
    f.write('        </color>\n')
    f.write('      </line_color>\n')
    f.write('    </widget>\n')
    f.write('    <widget type="polyline" version="2.0.0">\n')
    f.write('      <name>Polyline_1</name>\n')
    f.write('      <x>320</x>\n')
    f.write('      <y>25</y>\n')
    f.write('      <width>70</width>\n')
    f.write('      <height>1</height>\n')
    f.write('      <points>\n')
    f.write('        <point x="0.0" y="0.0">\n')
    f.write('        </point>\n')
    f.write('        <point x="70.0" y="0.0">\n')
    f.write('        </point>\n')
    f.write('      </points>\n')
    f.write('      <line_color>\n')
    f.write('        <color name="GROUP-BORDER" red="150" green="155" blue="151">\n')
    f.write('        </color>\n')
    f.write('      </line_color>\n')
    f.write('    </widget>\n')

    f.write('    <widget type="combo" version="2.0.0">\n')
    f.write('      <name>position</name>\n')
    f.write('      <pv_name>loc://PVID_Navigator_SystemExpert_position("Behind")</pv_name>\n')
    f.write('      <x>190</x>\n')
    f.write('      <y>190</y>\n')
    f.write('      <width>150</width>\n')
    f.write('      <items>\n')
    f.write('        <item>Behind</item>\n')
    f.write('        <item>Beneath</item>\n')
    f.write('      </items>\n')
    f.write('    </widget>\n')
    f.write('    <widget type="label" version="2.0.0">\n')
    f.write('      <name>pos_lbl</name>\n')
    f.write('      <text>Position policy:</text>\n')
    f.write('      <x>60</x>\n')
    f.write('      <y>190</y>\n')
    f.write('      <width>120</width>\n')
    f.write('      <height>30</height>\n')
    f.write('      <horizontal_alignment>2</horizontal_alignment>\n')
    f.write('      <vertical_alignment>1</vertical_alignment>\n')
    f.write('    </widget>\n')

    f.write('  </widget>\n')
